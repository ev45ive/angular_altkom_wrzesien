import { NgModel } from '@angular/forms/src/directives';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestingComponent } from './testing.component';

fdescribe('TestingComponent', () => {
  let component: TestingComponent;
  let fixture: ComponentFixture<TestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestingComponent ],
      imports:[
        CommonModule,
        FormsModule
      ],
      schemas:[

      ],
      providers:[{
        provide:'token', useValue:'value'
      }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should provide test service', () => {
    expect(component.value).toEqual('value');
  });

  it('should render text', () => {
    component.test = "Placki"
    fixture.detectChanges();
    
    expect(fixture.debugElement.query(By.css('p'))
          .nativeElement.textContent).toEqual("Placki");
  });


  // FIX: https://medium.com/@sevcsik/testing-ngmodel-in-angular-2-d9c79923f973
  it('should update test', fakeAsync(() => {
    let input = fixture.debugElement.query(By.css('input'))
    debugger;
    input.nativeElement.value = 'test3'
    input.triggerEventHandler('change',{
      value:'test2'
    })
    fixture.detectChanges();
    fixture.whenStable().then(()=>{
      expect(component.test).toEqual('value');
    })

  }));


});

import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'testing',
  template: `
    <p>{{test}}</p>
    <input type="text" [(ngModel)]="test">
  `,
  styles: []
})
export class TestingComponent implements OnInit {

  test = 'testing'

  constructor(@Inject('token') public value) { }

  ngOnInit() {
  }

}

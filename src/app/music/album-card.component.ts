import { Component, OnInit,Input, ViewEncapsulation } from '@angular/core';
import { Album, AlbumImage } from './interfaces'

@Component({
  selector: 'album-card,.album-card',
  template: `
    <img class="card-img-top" [src]="image.url">
    <div class="card-body">
      <!-- <input (input)="size = $event.target.value" type="range" max="30"> {{size}} -->
      <h4 class="card-title">{{album.name | shorten : 20 }}</h4>
    </div>
  `,
  styles: [`
    :host{
      flex: 1 1 24%;
      min-width: 24%;
      margin-bottom: .5em;
    }
  `],
  encapsulation: ViewEncapsulation.Emulated
})
export class AlbumCardComponent implements OnInit {

  @Input('album')
  set setAlbum(album){
    this.album = album;
    this.image = album.images[1]
  }

  album:Album;

  image:AlbumImage;

  constructor() { }

  ngOnInit() {
  }

}

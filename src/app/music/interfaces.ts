export interface Entity{
    id: string;
    name: string;
}

export interface Album extends Entity{
    artists: Artist[];
    images: AlbumImage[];
}

export interface Artist extends Entity{}

export interface AlbumImage{
    url: string;
    width: number;
    height: number;
}

import { Observable } from 'rxjs/Rx';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MusicService } from './music.service'
import { FormGroup, AbstractControl, FormControl, Validators, ValidatorFn, Validator, AsyncValidatorFn, ValidationErrors } from '@angular/forms'

@Component({
  selector: 'search-form',
  template: `
  <form [formGroup]="queryForm">
    <div class="form-group mb-2">
      <input type="text" class="form-control" formControlName="query" placeholder="Search for..." #queryRef>
      
      <div *ngIf="queryForm.pending">Please wait ... </div>

      <div *ngIf="queryForm.controls.query.touched || queryForm.controls.query.dirty">
        <div *ngIf="queryForm.controls.query.errors?.required">Pole wymagane</div>
        <div *ngIf="queryForm.controls.query.errors?.badword">
          No bat words, like "{{ queryForm.controls.query.errors?.badword }}" please!  
        </div>
        <div *ngIf="queryForm.controls.query.errors?.minlength">
          Wymagana dlugosc : {{queryForm.controls.query.errors.minlength.requiredLength}}
        </div>
      </div>
      <span class="input-group-btn" hidden>
        <button class="btn btn-secondary" type="button" (click)="search(queryRef.value)">Go!</button>
      </span>
    </div>
  </form>
  `,
  styles: [`
    form .ng-invalid.ng-touched,
    form .ng-invalid.ng-dirty {
      border: 2px solid red !important;
    }
  `]
})
export class SearchFormComponent implements OnInit {

  queryForm: FormGroup;

  constructor(private music: MusicService) {

    let censor = (text): ValidatorFn => {

      return (c: AbstractControl): ValidationErrors => {
        let error = c.value.indexOf(text) > -1;

        return error ? {
          'badword': text
        } : null;
      }
    }

    let asyncCensor = (text): AsyncValidatorFn => {
      //let error = c.value.indexOf(text) > -1;

      return (c: AbstractControl): Observable<ValidationErrors | null> => {
        // return new Promise( (resolve, reject) => { resolve(errors) } )

        return Observable.create(observable => {
          setTimeout(() => {
            let error = c.value.indexOf(text) > -1;

            observable.next(error ? {
              'badword': text
            } : null)
            
            observable.complete()
          }, 2000)
        })
      }
    }

    this.queryForm = new FormGroup({
      'query': new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        //censor('bat')
      ], [
        asyncCensor('bat')
      ]),
    })
    console.log(this.queryForm)
  }

  search(query) {
    this.music.search(query)
  }

  ngOnInit() {
    this.queryForm.get('query').valueChanges
      //.filter(query => this.queryForm.valid )
      .debounceTime(400)
      .distinctUntilChanged()
      .filter(query => query.length >= 3)
      .subscribe(query => {
        this.search(query)
      })
  }

}

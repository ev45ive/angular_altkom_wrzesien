import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http'
import { AuthService } from '../auth/auth.service'
import { Album } from './interfaces'

//import { Observable } from 'rxjs/Rx';
// import 'rxjs/add/operator/map'
// import 'rxjs/add/operator/catch'
// import 'rxjs/add/operator/retryWhen'
import { Observable } from 'rxjs/Observable'
//import 'rxjs/Observable/Of'
import 'rxjs/Rx'
import { Subject, BehaviorSubject } from 'rxjs/Rx'

@Injectable()
export class MusicService {

  constructor(private http:Http, 
              private auth:AuthService) {
  }

  albums$ = new BehaviorSubject<Album[]>([])

  search(query){
    let url = `https://api.spotify.com/v1/search`
    + `?q=${query}&type=album&market=PL`;

    this.http.get(url)
    .map(response => response.json())
    .map(data => <Album[]> data.albums.items)
    .catch(err => {
      this.auth.authenticate()
      return Observable.of([]) 
    })
    .subscribe(albums => {
      this.albums$.next(albums)
    })
  }

  getAlbums():Observable<Album[]>{
    return this.albums$.asObservable()
  }
}

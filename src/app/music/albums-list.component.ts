import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MusicService } from './music.service'
import { Album } from './interfaces'
import { Observable } from 'rxjs'

@Component({
  selector: 'albums-list',
  template: `
    <div class="card-deck"> 
        <div class="card album-card"
          [album]="album"
          *ngFor="let album of albums$ | async">
        </div>
    </div>
  `,
  styles: []
})
export class AlbumsListComponent implements OnInit {

  albums$: Observable<Album[]>;

  constructor(private music: MusicService) { }

  ngOnInit() {
      this.albums$ = this.music.getAlbums()
  }

}

import { ShortenPipe } from './shorten.pipe';

describe('ShortenPipe', () => {
  it('create an instance', () => {
    const pipe = new ShortenPipe();
    expect(pipe).toBeTruthy();

    expect(pipe.transform('Ala ma kota, a kot jest za dlugi',3)).toEqual('Ala')
  });
});

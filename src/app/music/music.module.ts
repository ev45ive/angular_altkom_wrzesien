import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { SearchFormComponent } from './search-form.component';
import { AlbumsListComponent } from './albums-list.component';
import { AlbumCardComponent } from './album-card.component';
import { MusicService } from './music.service';
import { HttpModule } from '@angular/http'
import { ReactiveFormsModule } from '@angular/forms';
import { ShortenPipe } from './shorten.pipe'
import { routing } from './music.routing';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    ReactiveFormsModule,
    routing
  ],
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    AlbumsListComponent,
    AlbumCardComponent,
    ShortenPipe
  ],
  // exports:[
  //   MusicSearchComponent
  // ],
  providers:[
    // { provide: MusicService, useClass: MusicService }
    MusicService
  ]
})
export class MusicModule { }

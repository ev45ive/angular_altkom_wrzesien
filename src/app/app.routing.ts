import { Injectable } from '@angular/core';
import { Playlist } from './playlists/playlist';
import { PlaylistsComponent } from './playlists/playlists.component';
import { ActivatedRouteSnapshot, CanActivate, Resolve, RouterModule, RouterStateSnapshot, Routes } from '@angular/router';
import { DetailsContainerComponent } from './playlists/details-container.component';
import { PlaylistsService } from './playlists/playlists.service'

@Injectable()
export class ResolvePlaylist implements Resolve<Playlist>{

    constructor(private service: PlaylistsService){}

    resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot){
        return this.service.getPlaylist(route.params['id'])
    }
}

export class GuardDetails implements CanActivate{
    canActivate(route:ActivatedRouteSnapshot, state:RouterStateSnapshot){
        return false
    }
}

const routes:Routes = [
    { path: '', redirectTo:'music', pathMatch:'full' /*'prefix'*/ },
    { path: 'playlists', component: PlaylistsComponent, children:[
        { path: '', component: DetailsContainerComponent },
        { path: ':id', component: DetailsContainerComponent,
            canActivate:[GuardDetails],
            data:{
                title: 'Playlist'
            },
            resolve:{
                'playlist': ResolvePlaylist
            } 
        }
    ] },
    
    { path: 'music', loadChildren:'./music/music.module.ts#MusicModule' },

    { path: '**', redirectTo:'playlists', pathMatch:'full' /*'prefix'*/ },
]

console.log(routes)

export const routing = RouterModule.forRoot(routes,{
    enableTracing: false,
    useHash: false
})
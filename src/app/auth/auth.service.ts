import { Injectable } from '@angular/core';
import { RequestOptions } from '@angular/http';

@Injectable()
export class AuthService {

  constructor(private requestOptions:RequestOptions) { }

  getToken(){
    let token = localStorage.getItem('token')

    if(!token){
      const match = window.location.hash.match(/#access_token=(.*?)\&/)
      token = match && match[1]
    }
    if(!token){
      return this.authenticate()
    }
    localStorage.setItem('token',token)

    this.requestOptions
        .headers
        .append('Authorization', 'Bearer ' + token)

    this.requestOptions
        .headers
        .append('Authorization', 'Bearer ' + token)

    return token;
  }

  authenticate(){
    localStorage.removeItem('token')

    const client_id = '770f6cffb8204f88a731343ad607bed7'
    const redirect_uri = 'http://localhost:4200/'
    const redirect = `https://accounts.spotify.com/authorize?`
    +`client_id=${client_id}`
    +`&response_type=token`
    +`&redirect_uri=${redirect_uri}`;
    
    window.location.replace(redirect)
  }

}

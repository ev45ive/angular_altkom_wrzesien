import { Directive, Input, TemplateRef, ViewContainerRef, ViewRef } from '@angular/core';

@Directive({
  selector: '[unless]'
})
export class UnlessDirective {

  constructor(private tpl:TemplateRef<any>, private vcr: ViewContainerRef ) { }

  cache:ViewRef;

  @Input('unless')
  set shouldHide(hide){
    if(hide){
      // this.vcr.clear()
      this.cache = this.vcr.detach()
    }else{
      if(this.cache){
        this.vcr.insert(this.cache)
      }else{
        this.vcr.createEmbeddedView(this.tpl)
      }
    }
  }

}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Playlist } from './playlist'


// [style.borderLeftColor]=" (hover == playlist?  playlist.color  : 'initial') "
// (mouseenter)=" hover = playlist "
// (mouseleave)=" hover = false  "
// [routerLink]="['playlists', playlist.id]"

@Component({
  selector: 'playlists-list',
  template: `
    <div class="list-group">
      <div *ngFor="let playlist of playlists" class="list-group-item"
        [class.active]="playlist === selected"  

        [highlight]="playlist.color" 

        (click)="pick(playlist)">
        {{ playlist.name }} 
      </div>
    </div>
  `,
  styles: [`
    .list-group-item{
      border-left: 5px solid black;
    }
  `]
})
export class PlaylistsListComponent implements OnInit {

  @Input()
  playlists: Playlist[]

  @Input()
  selected:Playlist;


  @Output()
  selectedChange = new EventEmitter<Playlist>()

  pick(playlist:Playlist){
    this.selectedChange.emit(playlist)
  }

  constructor() { }

  ngOnInit() {
  }

}

import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PlaylistsService } from './playlists.service'

@Component({
  template: `
    <playlist-details 
        *ngIf="(selected$ | async) as selected; else selectPlaylist" 
        [playlist]="selected">

      <h4>Playlist Details</h4>

      <input #handle >

    </playlist-details>
    
    <ng-template #selectPlaylist>
      <div> Please select playlist </div>
    </ng-template>
  `,
  styles: []
})
export class DetailsContainerComponent implements OnInit {

  selected$

  constructor(private service: PlaylistsService,
    private route: ActivatedRoute) {
    // let id = parseInt(this.route.snapshot.params['id']);
    // this.selected = service.getPlaylist(id)
  }

  ngOnInit() {
    this.selected$ = this.route.data
    .do( data => console.log(data) )
    .map( resolved => resolved['playlist'] )
    // this.selected$ = this.route.params
    //     .map( params => this.service.getPlaylist( parseInt(params['id']) ) )
  }

}

import { Observable } from 'rxjs/Rx';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Playlist } from './playlist'
import { PlaylistsService } from './playlists.service'
import { Router } from '@angular/router'

@Component({
  selector: 'playlists',
  template: `
      <div class="row">
          <div class="col">
            <playlists-list [playlists]="playlists$ | async"
              (selectedChange)="select($event.id)"></playlists-list>
          </div>
          <div class="col">
              <router-outlet></router-outlet>
          </div>
      </div>
  `,
  styles: [`
    /*:host{
      border:1px solid black;
      display:block;
    }

    :host >>> p{
      color: red;
    }*/
  `]
})
export class PlaylistsComponent implements OnInit {

  playlists$:Observable<Playlist[]>;
  
  constructor(
    private service:PlaylistsService, 
    public router:Router) {

    router.events.subscribe( event => console.log(event) )

    this.playlists$ = service.getPlaylists()
  }

  select(playlist_id){
    this.router.navigate(['playlists',playlist_id])
  }

  ngOnInit() {
   
  }


}

import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';

@Injectable()
export class PlaylistsService {

  playlists = [
    {
      id: 1,
      name: "Angular greatest Hits!",
      favourite: true,
      color: "#ff0000",
    },
    {
      id: 2,
      name: "Best of Angular",
      favourite: false,
      color: "#00ff00",
    },
    {
      id: 3,
      name: "Angular Playlist!",
      favourite: true,
      color: "#0000ff",
    }
  ]

  getPlaylist(id){
    return this.playlists.find( playlist => playlist.id == id )
  }

  onSave(save_playlist){
    let index = this.playlists.findIndex( ({id}) => id === save_playlist.id )
    this.playlists.splice(index,1,save_playlist)
  }

  getPlaylists(){
    return Observable.of(this.playlists)
  }

  constructor() { }

}

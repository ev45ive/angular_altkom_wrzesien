import { Directive, ElementRef, OnInit, Input, Attribute, HostListener,
HostBinding } from '@angular/core';

@Directive({
  selector: '[highlight]'
})
export class HighlightDirective implements OnInit {

  // public elem:ElementRef
  private secret = 'lubie placki'

  constructor(public elem: ElementRef) {
    //this.elem = elem
  }

  // <div [style.borderLeftColor]="color()"
  @HostBinding('style.borderLeftColor')
  get color(){
    return this.hover? this.highlightColor : 'initial'
  }

  @Input('highlight')
  highlightColor

  hover = false;

  // <div (mouseenter)="onHover($event.mouseX)"
  @HostListener('mouseenter', ['$event.mouseX'])
  onHover(mouseX: number) {
    this.hover = true
  }

  @HostListener('mouseleave', ['$event.mouseX'])
  onLeave(mouseX: number) {
    this.hover = false
  }

  ngOnInit() {
    //this.color = this.highlightColor
  }

}


import { Component, ContentChild, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Playlist } from './playlist'

@Component({
  selector: 'playlist-details',
  template: `
  <ng-content select="h4"></ng-content>

  <div *ngIf="mode == 'show'; else formTpl">
    <div class="form-group">
      <label>Name:</label>
      <div class="form-control-static">{{playlist.name}}</div>
    </div>
    <div class="form-group">
      <div class="form-control-static">{{playlist.favourite? 'Favourite' : '' }}</div>
    </div>
    <div class="form-group">
      <label>Color:</label>
      <div class="form-control-static" [style.color]="playlist.color">{{playlist.color}}</div>
    </div>
    <button  class="btn btn-success" (click)="edit()">Edit</button>
  </div>

  <ng-template #formTpl>
  <div>
    <div class="form-group">
      <label>Name:</label>
      <input class="form-control" type="text" #nameRef
          [(ngModel)]="playlist_copy.name">
    </div>
    <div class="form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" 
        [(ngModel)]="playlist_copy.favourite"> 
        Favourite 
      </label>
    </div>
    <div class="form-group">
      <label>Color:</label>
      <input type="color"
        [(ngModel)]="playlist_copy.color">
    </div>
  </div>
  <button  class="btn btn-success" (click)="save()">Save</button>
  </ng-template>


  <ng-content></ng-content>
  `,
  styles: [],
  // inputs:[
  //   'playlist:value'
  // ]
})
export class PlaylistDetailsComponent implements OnInit {

  @Input('playlist')
  set setPlaylist(playlist:Playlist){
    // ...
    this.playlist = playlist;
  }

  playlist:Playlist;

  playlist_copy:Playlist;

  @ViewChild('nameRef')
  nameRef

  @ContentChild('handle')
  handle

  ngAfterContentInit(){
    console.log(this.handle)
    this.handle.nativeElement.value = ";-)"
  }

  mode = 'show'

  ngAfterViewChecked(){
    //if(this.nameRef)
    //this.nameRef.nativeElement.focus()
  }

  @Output('saved')
  saved = new EventEmitter<Playlist>()
  
  save(){
    this.mode = 'show';
    this.saved.emit(this.playlist_copy)
  }

  edit(){
    this.mode = 'edit';
    this.playlist_copy = Object.assign({}, this.playlist);
  }

  constructor() { }

  ngOnInit() {
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ApplicationRef } from '@angular/core';
import { FormsModule } from '@angular/forms'

import { MusicModule } from './music/music.module'

import { AppComponent } from './app.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { PlaylistsListComponent } from './playlists/playlists-list.component';
import { PlaylistDetailsComponent } from './playlists/playlist-details.component';
import { HighlightDirective } from './playlists/highlight.directive';
import { UnlessDirective } from './playlists/unless.directive';

import { HttpModule } from '@angular/http';
import { AuthService } from './auth/auth.service'
import { routing, ResolvePlaylist, GuardDetails } from './app.routing';
import { DetailsContainerComponent } from './playlists/details-container.component'
import { PlaylistsService } from './playlists/playlists.service';
import { TestingComponent } from './testing.component'

@NgModule({
  declarations: [
    AppComponent,
    PlaylistsComponent,
    PlaylistsListComponent,
    PlaylistDetailsComponent,
    HighlightDirective,
    UnlessDirective,
    DetailsContainerComponent,
    TestingComponent
  ],
  // schemas:[
  //   CUSTOM_ELEMENTS_SCHEMA
  // ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    //MusicModule,
    routing
  ],
  providers: [
    AuthService,
    PlaylistsService,
    ResolvePlaylist,
    GuardDetails
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private appRef:ApplicationRef, private auth:AuthService){
    //console.log(appRef)
    auth.getToken()
  }
}
